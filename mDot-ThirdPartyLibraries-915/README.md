# mDot-ThirdPartyLibraries-915

## Importing 3rd party sensor libraries into your mDot program

### Preparation

Must already have mbed.org account set up. If that is not done, see the following documentation: [mDot-SetupAccount] (https://gitlab.op-bit.nz/BIT/Project/Internet-Of-Things/nodes/tree/master/mDot-SetupAccount)

## Importing library

Go to the webpage of the library you wish to use. For example: [https://developer.mbed.org/users/Julepalme/code/DHT22/](>https://developer.mbed.org/users/Julepalme/code/DHT22/)

On the right of the screen, click _Import into Compiler_.

![Import into compiler](resources/importintocompiler.jpg "Import into compiler")

In the compiler import popup window change the target folder to the folder of your program. Setting _import as_ should be _library_. Click Import.

![Import as a library](resources/importaslibrary.jpg "Import as a library")

## Include library

You need to include the library in your source code before you can use it. Near the top of your program's main cpp source file (usually main.cpp) file, with the other include statements add

`include "libraryName.h"`

![Include](resources/include.jpg "Include")
