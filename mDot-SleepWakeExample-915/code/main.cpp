
#include "mbed.h"
#include "mDot.h"
#include "ChannelPlans.h"
#include "MTSLog.h"


int main() {
    int count = 0;
    // use AU915 plan
    lora::ChannelPlan* plan = new lora::ChannelPlan_AU915();
    assert(plan);
    // get a mDot handle
    mDot* dot = mDot::getInstance(plan);
    assert(dot);
    
    //it will keep looping and sleeping in this loop - thread continues from sleep
    while (true) {
    count++;
    // print the message 
    logInfo("Hello world! I am an mDot counting %d", count);    
    // go to sleep and wake up automatically sleep_time seconds later 
    uint32_t sleep_time = 5;
    //false is "don't deep sleep" - mDot doesn't do that 
    dot->sleep(sleep_time, mDot::RTC_ALARM, false);
    printf("\r\n\r\n"); 
    }
}