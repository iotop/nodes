# mDot-HelloWorld-915

LoRa Version: https://os.mbed.com/users/GreensladeNZ/code/mDot-HelloWorld-ABP-AU915/

## Simplified mDotting - loading pre-made examples for DunedinIoT's mDots

Note: This should be much easier to follow than the initial *Getting Started* tutorial, as the *leg-work* has been done finding compatible version of libraries and the code was written and tested on our mDots, so I recommend starting here.

## Preparation

Assuming you have not already completed these steps at least once:

- Create an account at [https://developer.mbed.org](https://developer.mbed.org)
- Then click `Hardware`, then `Boards`

![List of hardware boards](resources/hardware-boards.jpg "List of hardware boards")

- Filter *platform vendor* by Multitech

![Filter by platform vendor](resources/filterplatformvendor.jpg "Filter by platform vendor")

- Select mDot

![Select mDot device](resources/select-mdot.jpg "Select mDot device")

- On the page that loads, on the right, click the *Add to your embed compiler* button.

## Importing our example program
        
- For this example we're using a basic *Hello World* program that simply prints a count over the USB/Serial debug output. The code for this project is available from: [https://os.mbed.com/users/kellybs1/code/mDot_helloworld_bk/](https://os.mbed.com/users/kellybs1/code/mDot_helloworld_bk/)

![Find the hello world program](resources/findhelloworldbk.jpg "Find the hello world program")

- On the right of the screen, click *Import into Compiler*

![Import the project into the compiler](resources/importintocompiler.jpg "Import the project into the compiler")

- In the Compiler click to import the program as a program.

![Import the program](resources/importingasprogram.jpg "Import the program")

- Click *Compile* and download the bin file when prompted.

![Save the compiled program](resources/savebin.jpg "Save the compiled program")

## Load program onto mDot

- Attach the mDot to the UDK board, and attach the UDK to a PC via USB
- The mDot should appear in the pc as a flash drive would for example *MULTITECH (F:)*
- Copy the binary file to this drive
- The mDot might automatically restart automatically, but if it does not, press the Reset button on the UDK

## Seeing debug info on a PC over USB

- On Windows you must install serial-USB driver from here: [http://www.st.com/en/embedded-software/stsw-link009.html](http://www.st.com/en/embedded-software/stsw-link009.html)
- Open a serial terminal. I used the Arduino IDE's serial monitor set to the correct COM port (this varies depending on the machine) at 9600 baud
- If the application runs correctly and the serial monitor is configured correctly you should see debugging output

![Debugging output](resources/HelloWorld.jpg "Debugging output")
