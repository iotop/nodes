/*************************************
 * This simple example program prints 
 * a message on the USB debug port.
 ************************************/

#include "mbed.h"

int main() {
    int count = 0;
    // print the message on 3s interval
    while (true) {
        count++;
        printf("Hello world! I am an mDot and I count %d\r\n", count);
        wait(3);
    }
}