# Dragino-HelloWorld-AU915

### Configuring a Dragino LoRa Shield for use on the AU915 frequency 

<br />
![draginoNode.jpg](resources/draginoNode.jpg)
<br />

## Preparation

- Download and install the Arduino IDE.

Make sure you have version 1.6.6 (or above) of the Arduino IDE. The documentation for the MCCI Arduino-LMIC library state that this version is required because it requires C99 mode to be enabled by default.

## Required hardware

- Dragino LoRa Shield AU915
- Arduino Uno board
- antenna

## Download the Arduino-LMIC library

- Open a web browser and navigate to the [MCCI Arduino-LMIC library](https://github.com/thomaslaurenson/arduino-lmic) repository
- Click on the _Clone or Download_ button
- Select _Download ZIP_

<br />
![download.png](resources/download.png)
<br />

## Add the MCCI Arduino-LMIC library to the _libraries_ directory of your Arduino IDE installation

- Open Arduino IDE
- Navigate to _Sketch_, _Include Library_, _Add .ZIP Library_
- Find the ZIP file, and select Open

<br />
![addLibrary.png](resources/addLibrary.png)
<br />

## Adding the Example Sketch 

- Add the provided sketch for the Dragino LoRa Shield from the `examples` folder into the Arduino Sketchbook folder. 

<br />
![addSketch.png](resources/addSketch.png)
<br />

The example sketch is provided in the `examples` directory. The sketch is named:

<br />

```
ttn-abp-dragino-lorashield-au915.ino
```
<br />

- Make sure to put the sketch (`.ino` file) into a folder with the same name.

<br />
![sketchFolder.png](resources/sketchFolder.png)
<br />

- Open the sketch for your device
- Set the ABP Keys and Device Address

Make sure you remember to **change the ABP properties in the sketch file**. You need to replace the `FILLMEIN` placeholders with the network session key, application session key and device address. The block of code is displayed for reference below.

<br />

```
// LoRaWAN NwkSKey, network session key
static const PROGMEM u1_t NWKSKEY[16] = { FILLMEIN };

// LoRaWAN AppSKey, application session key
static const u1_t PROGMEM APPSKEY[16] = { FILLMEIN };

// LoRaWAN end-device address (DevAddr)
static const u4_t DEVADDR = FILLMEIN; // <-- Change this address for every node!
```
<br />

- Change your COM port to the attached device

<br />
![port.png](resources/port.png)
<br />

- The sketch is ready to be compiled and uploaded by clicking on _Upload_

<br />
![board.png](resources/board.png)
<br />

## Checking Functionality

- Make sure you have the correct COM port selected
- Make sure you have the right board selected
- Check if the Serial Monitor is set to `115200`

<br />
![baud.png](resources/baud.png)
<br />

- Click on _Serial Monitor_

<br />
![serialMonitor.png](resources/serialMonitor.png)
<br />	

- You should see output similar to the listing below:

<br />

```
Starting
Loading AU915 Configuration...
6853: EV_TXSTART
Packet queued
Sending packet on frequency:917400000
256551: EV_TXCOMPLETE (includes waiting for RX windows)
4006984: EV_TXSTART
Packet queued
Sending packet on frequency:916800000
4277002: EV_TXCOMPLETE (includes waiting for RX windows)
8027439: EV_TXSTART
Packet queued
Sending packet on frequency:917200000
8275202: EV_TXCOMPLETE (includes waiting for RX windows)
```
<br />