﻿#  mDot-PlantMonitor-AU915 

## mDot using a DHT22, LDR and Soil Moisture Sensor

#### Prep

Must already have mbed.org account set up. If that is not done, see the Prep section of this file: https://gitlab.op-bit.nz/BIT/Project/Internet-Of-Things/nodes/tree/master/mDot-HelloWorld-915

#### Required Hardware
mDot - 915 MultiTech mDot UDK board DHT22 Sensor 10 KOhm resistor 330Ohm resistor, standard light dependent resistor and a sparkfun soil moisture sensor 

#### Wiring 

Using the corresponding Arduino pins on the UDK and some photos below of what the boards should look like

![alt text](resources/all_sensor_fritz_bb.jpg)

![alt text](resources/all_sensor_pic1.jpg)

![alt text](resources/all_sensor_pic2.jpg)

![alt text](resources/all_sensor_pic3.jpg)

#### Importing the code 

Go to https://os.mbed.com/users/lootspk/code/mDot-PlantMonitor-AU915/ and from here you can import the code into the compiler and upload 
it to the board, click import into compiler to make it editable by you and able to be compiled

![alt text](resources/importintocompiler.jpg)

Once in the compiler click "import as program"

![alt text](resources/importasprogram.jpg)

Near the top of the program, change the network setting values to match the settings required for your LoRaWAN.

![alt text](resources/networksettings.jpg)

If adaptive data rate is disabled, ie, ```static bool adr = false;```

Scroll down to ```dot->setTxDataRate(mDot::DR2);``` and set your required data rate.

Click “Compile” and download the bin file when prompted.

![alt text](resources/savebin.jpg)

##### Load program onto mDot

Attach the mDot to the UDK board, and attach the UDK to a PC via USB.
The mDot should appear in the pc as a flash drive would for example “MULTITECH (F:)”.

Copy the binary file to this drive. The mDot usually would automatically restart, but if it does not, press the Reset button on the UDK.

#####  Seeing debug info on a PC over USB

On Windows you must install serial-USB driver from here: http://www.st.com/en/embedded-software/stsw-link009.html

Open a serial terminal. I used the Arduino IDE’s serial monitor set to the correct COM port (this varies depending on the machine) at 9600 baud.

If the application runs correctly and the serial monitor is configured correctly you should see debugging output, and be receiving packets at the gateway.