# mDot-SetupAccount

## Setting up an mbed account for development

Execute the follow steps:

- Create an account at [https://developer.mbed.org](https://developer.mbed.org)
- Then click `Hardware`, then `Boards`
- Filter *platform vendor* by Multitech
- Select mDot
- On the page that loads, on the right, click the *Add to your embed compiler* button