# Dragino-TempHumidityLDR-868

##  Dragino LoRa shield (868MHz) temperature/humidity/light level example for IoT Dunedin server and 868MHz iC880a based gateway

As displayed as Mid-year showcase 2017

![Dragino868TempHumidLDR_real](resources/Dragino868TempHumidLDR_real.jpg "Dragino868TempHumidLDR_real")


## Hardware required

- Arduino Uno or compatible
- Dragino 868Mhz LoRa Arduino shield 
- 10KOhm resistor
- 330 Ohm resistor
- Standard light dependent resistor (LDR)

## Additional software libraries

Adafruit DHT library is available in the [DHT-sensor-library directory](https://gitlab.op-bit.nz/BIT/Project/Internet-Of-Things/nodes/blob/master/Dragino-TempHumidity-868/DHT-sensor-library)

## Wiring

Note: Dragino shield not included in diagram but must be installed
Note: Dragino shield leaves pins 0,1,4,5,6 for use

![Dragino868TempHumidLDR_bb](resources/Dragino868TempHumidLDR_bb.png "Dragino868TempHumidLDR_bb")

## Code

![Dragino868TempHumidLDR.ino](resources/Dragino868TempHumidLDR.ino "Dragino868TempHumidLDR.ino")