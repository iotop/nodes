//with the CO2 sensor code, simply copy the code that relates to either loop or set up it this file's loop and set up. Should only need to change one thing
//(indicated in comment line below)


/************************** Configuration ***********************************/
#include <TinyLoRa.h>
#include <SPI.h>
#include <Adafruit_CircuitPlayground.h>
#include <Arduino.h>
#define ANALOGPIN A0 
unsigned long myMHZ19Timer = 0;   

// Visit your thethingsnetwork.org device console
// to create an account, or if you need your session keys.

// Network Session Key (MSB)
uint8_t NwkSkey[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

// Application Session Key (MSB)
uint8_t AppSkey[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

// Device Address (MSB)
uint8_t DevAddr[4] = { 0x00, 0x00, 0x00, 0x00 };

/************************** Example Begins Here ***********************************/
// Data Packet to Send to TTN
//unsigned char loraData[11] = {"hello LoRa"}; //insteed of hello loRa place whatever code is printing the data to screen
float adjustedADC = analogRead(A0);    
byte payloadC = adjustedADC;

// How many times data transfer should occur, in seconds
const unsigned int sendInterval = 10;

// Pinout for Adafruit Feather 32u4 LoRa
TinyLoRa lora = TinyLoRa(7, 8, 4);

// Pinout for Adafruit Feather M0 LoRa
//TinyLoRa lora = TinyLoRa(3, 8, 4);

void setup()
{
  delay(2000);
  Serial.begin(9600);
  while (! Serial);
  
  // Initialize pin LED_BUILTIN as an output
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(ANALOGPIN, INPUT);                         // Pullup A0
  Serial.print("\nUsing Pin: ");                            // Print Raw Pin Number
  Serial.println(ANALOGPIN);
  
  // Initialize LoRa
  Serial.print("Starting LoRa...");
  // define multi-channel sending
  lora.setChannel(MULTI);
  // set datarate
  lora.setDatarate(SF7BW125);
  if(!lora.begin())
  {
    Serial.println("Failed");
    Serial.println("Check your radio");
    while(true);
  }
  Serial.println("OK");
}

void loop()
{
   if (millis() - myMHZ19Timer >= 1000)                      
    {
        Serial.println("-----------------"); 

        float adjustedADC = analogRead(A0);  
       
         
                    
        Serial.print("Analog raw: ");
        Serial.println(adjustedADC);
       

        adjustedADC = 6.4995 * adjustedADC - 580.53; // format; y=mx+c
        uint16_t co2value =  adjustedADC;
        
//        var value = (bytes[0] + (bytes[1] << 8)); 
        Serial.print("Analog CO2: ");      
        Serial.println(adjustedADC); 
         Serial.println(co2value);                                                 
        myMHZ19Timer = millis();

        
        
            Serial.println("Sending LoRa Data...");
            lora.sendData(co2value, sizeof(co2value), lora.frameCounter);
            Serial.print("Frame Counter: ");Serial.println(lora.frameCounter);
            lora.frameCounter++;


            // blink LED to indicate packet sent
            digitalWrite(LED_BUILTIN, HIGH);
            delay(1000);
            digitalWrite(LED_BUILTIN, LOW);
            
            Serial.println("delaying...");
            delay(sendInterval * 500);
    }
        
}
