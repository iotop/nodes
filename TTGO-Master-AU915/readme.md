## TTGO ESP32 T3 2.1_1.6

### Arduino IDE Setup

The following instructions will allow you to program the TTGO LoRa32 board with the Arduino IDE.

1. Install the current upstream Arduino IDE at the 1.8 level or later. The current version is at the [Arduino website](http://www.arduino.cc/en/main/software).
2. Start Arduino and open Preferences window.
3. Enter `https://dl.espressif.com/dl/package_esp32_index.json` into *Additional Board Manager URLs* field. You can add multiple URLs, separating them with commas.
4. Open Boards Manager from Tools > Board menu and install the `esp32` platform
5. Once installed, select `Heltec WiFi LoRa 32` as your current board. 
6. Make sure the correct COM port is selected, you should now be able to upload sketches!

If you wish to test your configuration, try uploading 'blink' onto the connected device. If successful, you should see the onboard green LED flashing.

#### Board detail

LED pin = GPIO(25).

#### Voltage Divider 

Pin: GPIO(35).
Type: 100k/100k.

#### OLED Display
Pins for the OLED display:
```
OLED_SDA (21)
OLED_SCL (22)
OLED_RST U8X8_PIN_NONE
```
OLED display libary option/model:
```
HAS_DISPLAY U8X8_SSD1306_128X64_NONAME_HW_I2C
```

#### Lora Chip
Pins for LORA chip SPI interface, reset line and interrupt lines:

```
LORA_SCK  (5)  
LORA_CS   (18)  
LORA_MISO (19)  
LORA_MOSI (27)  
LORA_RST  (23)  
LORA_IRQ  (26)  
LORA_IO1  (33)  
LORA_IO2 (32)  
```
Model: 
```
sx1276 HPD13A
```

#### SD Card slot

Pins for SD Card slot:

```
SD3-CS-IO13
CMD-MOS1-IO15 
CLK-SCK-IO14 
SD0-MISO-IO2 
DATA2:IO12 
DATA2:IO4 
```