/*
This program:
 - connects to a TTN by ABP/MANUAL
 - reads a switch for status on or off
 - sends the data onto the TTN when status of switch changes or after 20 seconds of unchanged status
 - sets the mDot to sleep
 - repeats these operations in a loop
*/

// import all required libraries
#include "mbed.h"
#include "mDot.h"
#include "ChannelPlans.h"
#include "MTSLog.h"
#include "dot_util.h"
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>

 

 //analogue ldr pin (A0 on UDK 2)
 DigitalIn gate_state(PA_3);  //define D0 as PA_3 as a digital input


// these options must match the settings on your Conduit

//device address (please change according to your own device registered on TTN server)
static uint8_t network_address[] = { FILL ME IN };
//network session key
static uint8_t network_session_key[] = { FILL ME IN };
//application sesssion or data session key
static uint8_t data_session_key[] = { FILL ME IN };
static uint8_t frequency_sub_band = 2; //VFI
static bool public_network = true;
//enable receipt of ackknowledge packets 0 = No, 1 = Yes
static uint8_t ack = 0;
//adaptive data rate enabler
static bool adr = false;
static bool oldstate;


//USB serial 
Serial pc(USBTX, USBRX);

//get ourselves an mDot pointer - we will assign to it in main()
mDot* dot = NULL;

//converts value to string
template <typename T>
string ToString(T val) {
    stringstream stream;
    stream << val;
    return stream.str();
}

int main() {
    gate_state.mode(PullUp);  
    //setting serial rate
    pc.baud(9600);
    
    // use AU915 plan
    lora::ChannelPlan* plan = new lora::ChannelPlan_AU915();
    assert(plan);
    // get a mDot handle with the plan we chose
    dot = mDot::getInstance(plan);
    assert(dot); 
    
    if (!dot->getStandbyFlag()) {
        logInfo("mbed-os library version: %d", MBED_LIBRARY_VERSION);

        // start from a well-known state
        logInfo("defaulting Dot configuration");
        dot->resetConfig();
        dot->resetNetworkSession();

        // make sure library logging is turned on
        dot->setLogLevel(mts::MTSLog::DEBUG_LEVEL);

        // update configuration if necessary
        if (dot->getJoinMode() != mDot::MANUAL) {
            logInfo("changing network join mode to MANUAL");
            if (dot->setJoinMode(mDot::MANUAL) != mDot::MDOT_OK) {
                logError("failed to set network join mode to MANUAL");
            }
        }
        // in MANUAL join mode there is no join request/response transaction
        // as long as the Dot is configured correctly and provisioned correctly on the gateway, it should be able to communicate
        // network address - 4 bytes (00000001 - FFFFFFFE)
        // network session key - 16 bytes
        // data session key - 16 bytes
        // to provision your Dot with a Conduit gateway, follow the following steps
        //   * ssh into the Conduit
        //   * provision the Dot using the lora-query application: http://www.multitech.net/developer/software/lora/lora-network-server/
        //      lora-query -a 01020304 A 0102030401020304 <your Dot's device ID> 01020304010203040102030401020304 01020304010203040102030401020304
        //   * if you change the network address, network session key, or data session key, make sure you update them on the gateway
        // to provision your Dot with a 3rd party gateway, see the gateway or network provider documentation
        update_manual_config(network_address, network_session_key, data_session_key, frequency_sub_band, public_network, ack);


        // enable or disable Adaptive Data Rate
        dot->setAdr(adr);
        
        //* AU915 Datarates
        //* ---------------
        //* DR0 - SF10BW125 -- 11 bytes
        //* DR1 - SF9BW125 -- 53 bytes
        //* DR2 - SF8BW125 -- 129 byte
        //* DR3 - SF7BW125 -- 242 bytes
        //* DR4 - SF8BW500 -- 242 bytes
        dot->setTxDataRate(mDot::DR2);
        
        // save changes to configuration
        logInfo("saving configuration");
        if (!dot->saveConfig()) {
            logError("failed to save configuration");
        }

        // display configuration
        display_config();
    } else {
        // restore the saved session if the dot woke from deepsleep mode
        // useful to use with deepsleep because session info is otherwise lost when the dot enters deepsleep
        logInfo("restoring network session from NVM");
        dot->restoreNetworkSession();
    }
    
    //this is where the magic happens
   
    while (true) {

        //init data variable
        std::vector<uint8_t> data;
        logInfo("Starting reed sensor readings");
        string reed_str = "";
        
        //switch default state is low if not pressed
        if (gate_state==0)
        {
           logInfo("/n");
           logInfo("Gate is Open!"); 
           logInfo("/n");
           reed_str = ToString(0);
           oldstate = gate_state;
        }
        else 
        {
           logInfo("/n");
           logInfo("Gate is Close!"); 
           logInfo("/n");
           reed_str = ToString(1);
           oldstate = gate_state;          
        }
        
        //build our output string now
      //  string output = "1" + reed_str;  
        string output = reed_str;  //output only 1 digit to minimise payload.  
    
        //serial output for debugging
        logInfo("Sending %s", output.c_str());
        
        // format data for sending to the gateway
        for (std::string::iterator it = output.begin(); it != output.end(); it++)
            data.push_back((uint8_t) *it);
    
        //now send
        send_data(data);
        logInfo("Data sent!");
        
        //check if the state is unchanged
        if (oldstate==gate_state)
        {
          // Set sleeptime to be 20 seconds if status unchanged
            uint32_t sleep_time = 20;
            //false is "don't deep sleep" - mDot doesn't do that 
            sleep_wake_rtc_or_interrupt(false);
        }
        
    }
}

