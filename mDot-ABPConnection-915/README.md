# mDot-ABPConnection-915

## Using an mDot to connect to a LoRa network over ABP (Activation by Personalisation)

### Preparation

Must already have mbed.org account set up. If that is not done, see the following documentation: [mDot-SetupAccount] (https://gitlab.op-bit.nz/BIT/Project/Internet-Of-Things/nodes/tree/master/mDot-SetupAccount)

## Required Hardware

- mDot - 915
- MultiTech mDot UDK board
- 900MHz antenna

## Importing our example program

- Go to: [https://developer.mbed.org/users/kellybs1/code/mDot_LoRa_Connect_ABPA/](https://developer.mbed.org/users/kellybs1/code/mDot_LoRa_Connect_ABPA/)

On the right of the screen, click _Import into Compiler_.

![Import into compiler](resources/importintocompiler.jpg "Import into compiler")

In the Compiler click to import the program as a program.

![Import as a program](resources/importasprogram.jpg "Import as a program")

Near the top of the program, change the network setting values to match the settings required for your LoRaWAN.

![LoRaWAN network settings](resources/networksettings.jpg "LoRaWAN network settings")

If adaptive data rate is disabled. For example: `static bool adr = false;`

Scroll down to: `dot->setTxDataRate(mDot::DR2);`

Set your required data rate here.

Click _Compile_ and download the bin file when prompted.

![Save compiled binary](resources/savebin.jpg "Save compiled binary")

## Load Program onto mDot

- Attach the mDot to the UDK board, and attach the UDK to a PC via USB
- The mDot should appear in the pc as a flash drive would for example *MULTITECH (F:)*
- Copy the binary file to this drive. The mDot might automatically restart automatically, but if it does not, press the Reset button on the UDK.

## Seeing debug info on a PC over USB

- On Windows you must install serial-USB driver from here: [http://www.st.com/en/embedded-software/stsw-link009.html](http://www.st.com/en/embedded-software/stsw-link009.html)
- Open a serial terminal. I used the Arduino IDE's serial monitor set to the correct COM port (this varies depending on the machine) at 9600 baud
- If the application runs correctly and the serial monitor is configured correctly you should see debugging output
