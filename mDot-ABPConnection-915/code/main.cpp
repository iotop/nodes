#include "mbed.h"
#include "mDot.h"
#include "ChannelPlans.h"
#include "MTSLog.h"
#include "dot_util.h"
#include <string>
#include <vector>
#include <algorithm>


//device address
static uint8_t network_address[] = { FILLMEIN };
//network session key
static uint8_t network_session_key[] = { FILLMEIN };
//application sesssion or data session key
static uint8_t data_session_key[] = { FILLMEIN };
static uint8_t frequency_sub_band = 2; //VFI
static bool public_network = true;
//enable receipt of ackknowledge packets 0 = No, 1 = Yes
static uint8_t ack = 0;
//adaptive data rate enabled
static bool adr = false;

Serial pc(USBTX, USBRX);

mDot* dot = NULL;

int main() {
    pc.baud(9600);
    std::vector<uint8_t> data;
    std::string data_str = "hello world";
    
    // use AU915 plan
    lora::ChannelPlan* plan = new lora::ChannelPlan_AU915();
    assert(plan);
    // get a mDot handle
    dot = mDot::getInstance(plan);
    assert(dot);
      
    if (!dot->getStandbyFlag()) {
        logInfo("mbed-os library version: %d", MBED_LIBRARY_VERSION);

        // start from a well-known state
        logInfo("defaulting Dot configuration");
        dot->resetConfig();
        dot->resetNetworkSession();

        // make sure library logging is turned on
        dot->setLogLevel(mts::MTSLog::DEBUG_LEVEL);

        // update configuration if necessary
        if (dot->getJoinMode() != mDot::MANUAL) {
            logInfo("changing network join mode to MANUAL");
            if (dot->setJoinMode(mDot::MANUAL) != mDot::MDOT_OK) {
                logError("failed to set network join mode to MANUAL");
            }
        }
        // in MANUAL join mode there is no join request/response transaction
        // as long as the Dot is configured correctly and provisioned correctly on the gateway, it should be able to communicate
        // network address - 4 bytes (00000001 - FFFFFFFE)
        // network session key - 16 bytes
        // data session key - 16 bytes
        // to provision your Dot with a Conduit gateway, follow the following steps
        //   * ssh into the Conduit
        //   * provision the Dot using the lora-query application: http://www.multitech.net/developer/software/lora/lora-network-server/
        //      lora-query -a 01020304 A 0102030401020304 <your Dot's device ID> 01020304010203040102030401020304 01020304010203040102030401020304
        //   * if you change the network address, network session key, or data session key, make sure you update them on the gateway
        // to provision your Dot with a 3rd party gateway, see the gateway or network provider documentation
        update_manual_config(network_address, network_session_key, data_session_key, frequency_sub_band, public_network, ack);
     
        // enable or disable Adaptive Data Rate
        dot->setAdr(adr);
        
        //* AU915 Datarates
        //* ---------------
        //* DR0 - SF10BW125 -- 11 bytes
        //* DR1 - SF9BW125 -- 53 bytes
        //* DR2 - SF8BW125 -- 129 byte
        //* DR3 - SF7BW125 -- 242 bytes
        //* DR4 - SF8BW500 -- 242 bytes
        dot->setTxDataRate(mDot::DR2);
        
        
        // save changes to configuration
        logInfo("saving configuration");
        if (!dot->saveConfig()) {
            logError("failed to save configuration");
        }

        // display configuration
        display_config();
    } else {
        // restore the saved session if the dot woke from deepsleep mode
        // useful to use with deepsleep because session info is otherwise lost when the dot enters deepsleep
        logInfo("restoring network session from NVM");
        dot->restoreNetworkSession();
    }
    
    // format data for sending to the gateway
    for (std::string::iterator it = data_str.begin(); it != data_str.end(); it++)
        data.push_back((uint8_t) *it);

    while (true) {
    
        send_data(data);

        //wait before resending
        wait(10);
    }
 
    return 0;
}
