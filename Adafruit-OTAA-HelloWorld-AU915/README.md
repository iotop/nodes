# Adafruit-OTAA-HelloWorld-AU915

### Configuring an Adafruit Feather M0 for use on the AU915 frequency 

<br />
![adafruitNode.jpg](resources/adafruitNode.jpg)
<br />

## Preparation

- Download and install the Arduino IDE. You can download it from [here] (https://www.arduino.cc/en/Main/Software). Make sure you have version 1.8 (or above) of the Arduino IDE.

## Arduino IDE Setup

- Start the Arduino IDE and navigate to the Preferences menu

![ideSetup.png](resources/ideSetup.png)

- Copy and paste the link below into the Additional Boards Manager URLs option in the Arduino IDE preferences and click OK to save the new preferences settings

`https://adafruit.github.io/arduino-board-index/package_adafruit_index.json`

![ideSetup2.png](resources/ideSetup2.png)

- Open the Boards Manager by navigating to the _Tools->Board_ menu

![ideSetup3.png](resources/ideSetup3.png)

- Install the latest Arduino SAMD Boards (version 1.6.18 or later)

![ideSetup4.png](resources/ideSetup4.png)

- Install the Adafruit SAMD package to add the board file definitions

![ideSetup5.png](resources/ideSetup5.png)

- Quit and reopen the Arduino IDE to ensure that all of the boards are properly installed

## Required hardware

- Adafruit Feather M0 LoRa RFM9x
- Breadboard
- Antenna - 3 inches or 7.8 cm
- 2 yellow wires

## Download the Arduino-LMIC library

- Open a web browser and navigate to the [MCCI Arduino-LMIC library](https://github.com/thomaslaurenson/arduino-lmic) repository
- Click on the _Clone or Download_ button
- Select _Download ZIP_

<br />
![download.png](resources/download.png)
<br />

## Add the MCCI Arduino-LMIC library to the _libraries_ directory of your Arduino IDE installation

- Open Arduino IDE
- Navigate to _Sketch_, _Include Library_, _Add .ZIP Library_
- Find the ZIP file, and select Open

<br />
![addLibrary.png](resources/addLibrary.png)
<br />

## Connecting it up

To send the "Hello World!" message you need to add two connections on the Feather board, and an antenna. The connections needed are:

<br />
![pinMapping.png](resources/pinMapping.png)
<br />

## Adding the Example Sketch 

- Add the provided sketch for the Adafruit Feather M0 from the `code` folder into the Arduino Sketchbook folder. 

<br />
![addSketch.png](resources/addSketch.png)
<br />

The example sketch is provided in the `code` directory. The sketch is named:

<br />

```
AdafruitFeatherM0-OTAA-HelloWorld-AU915.ino
```
<br />

- Make sure to put the sketch (`.ino` file) into a folder with the same name.

<br />
![sketchFolder.png](resources/sketchFolder.png)
<br />

- Open the sketch for your device
- Set the OTAA Keys

Make sure you remember to **change the OTAA properties in the sketch file**. You need to replace the `FILLMEIN` placeholders with a valid Network Session Key (NWKSKEY), Application Session Key (APPSKEY) and Device Address (DEVADDR). The block of code is displayed for reference below.

<br />

```
// This EUI must be in little-endian format, so least-significant-byte
// first. When copying an EUI from ttnctl output, this means to reverse
// the bytes. For TTN issued EUIs the last bytes should be 0xD5, 0xB3,
// 0x70.
static const u1_t PROGMEM APPEUI[8]= { FILLMEIN };
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}

// This should also be in little endian format, see above.
static const u1_t PROGMEM DEVEUI[8]= { FILLMEIN };
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}

// This key should be in big endian format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In
// practice, a key taken from the TTN console can be copied as-is.
static const u1_t PROGMEM APPKEY[16] = { FILLMEIN };
void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}
```
<br />

Also, make sure to write it in little endian format for the first two keys.
For example, if the key is `1122334455667788`:

`1122334455667788` - > `8877665544332211` -> `0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11`

- Change the COM port to the attached device

<br />
![port.png](resources/port.png)
<br />

- Select the _Adafruit Feather M0_ board (not the _Express_ one) by navigating to the _Tools->Board_ menu

![ideSetup6.png](resources/ideSetup6.png)


- The sketch is ready to be compiled and uploaded by clicking on _Upload_. Then wait until it shows _Done uploading_ at the bottom

<br />
![upload.png](resources/upload.png)
<br />

## Checking Functionality

- Make sure you have the correct COM port selected
- Make sure you have the right board selected
- Check if the Serial Monitor is set to `1152000`

<br />
![baud.png](resources/baud.png)
<br />

- Click on _Serial Monitor_ when _Done uploading_ is shown at the bottom

<br />
![serialMonitor.png](resources/serialMonitor.png)
<br />	

- You should see output similar to the listing below:


<br />

```
Starting...
Loading AU915/AU921 Configuration...

54051: EV_TXSTART
Packet queued
Sending packet on frequency: 917000000
121403: EV_TXCOMPLETE (includes waiting for RX windows)
434070: EV_TXSTART
Packet queued
Sending packet on frequency: 917500000
499514: EV_TXCOMPLETE (includes waiting for RX windows)
812184: EV_TXSTART
Packet queued
Sending packet on frequency: 917500000
877560: EV_TXCOMPLETE (includes waiting for RX windows)
```
<br />



