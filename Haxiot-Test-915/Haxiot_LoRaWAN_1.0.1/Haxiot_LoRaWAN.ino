
#include "LoRaWAN.h"
#include <SoftwareSerial.h>

#define ON 1
#define OFF 0

//************** Device parameters for registration *****************
char DEVICE_ADDR[] = "00610942";  //4 bytes required
char NWK_SESSION_KEY[] = "45767E9BE417A7829FAC4CFDD96627C3"; // 16 bytes required
char APP_SESSION_KEY[] = "BC5DD8F86361CB1CFFE69E8585BFE9DA"; //16 bytes required
char APP_KEY[] = "0AA5AF9F0B74FF519D7FB0FF57D3EB9C"; //16 bytes required
char APP_EUI[] =  "0000000000000000";  //8 bytes required

char txData[]="01020304050";      //sample data to be transmitted - must be hexadecimal values

LoRaWAN LoRaWAN(5,4); // Software Serial RX, TX   ** Set to 10, 11 for Mega2560 boards, Set to 5, 4 for Uno 
void setup() {


delay(1000);                //startup delay - gives Lora module time to reset if cold start
Serial.begin(9600);        //terminal serial port, 9600 = Default, 57600 = Haxiot shield

//LoRaWAN.LoRaResetModule();                          //restore module to factory default settings and reset device
LoRaWAN.LoRaDeviceEUI();                            //retrieves EUI from module and auto sets 
LoRaWAN.LoRaDevAddr(DEVICE_ADDR);                   //sets device addresss
LoRaWAN.LoraApplicationEUI(APP_EUI);                //sets application EUI
LoRaWAN.LoRaNetworkSessionKey(NWK_SESSION_KEY);     //sets network session key
LoRaWAN.LoRaApplicationSessionKey(APP_SESSION_KEY); //sets application session key
LoRaWAN.LoRaApplicationKey(APP_KEY);                //sets application key
LoRaWAN.LoRaAdaptiveDataRate(ON);      //set to ON to enable, OFF to disable
LoRaWAN.LoRaTXPower(20);                //dBM 10(Min), 12, 14, 16, 18, 20(Max)
LoRaWAN.LoRaChannelPlan(0,7);          //(start channel, end channel) - channel range 0 to 63
LoRaWAN.LoRaSaveConfig();                           //save configuration settings to module eeprom

//************* Connect to LoRaWAN Network Server****************//
// Comment out 
LoRaWAN.LoRaJoinABP ();       //Connect with Access By Personalization (ABP) networks
//LoRaWAN.LoRaJoinOTAA ();    //Connect with Over The Air Activation (OTAA) networks 


LoRaWAN.LoRaShowConfig();             //display current module configuration for debugging only

}

void loop() {
 

  //*************** Display received data from LoRaWAN 
  while (LoRaWAN.loraSerial.available()) {
    Serial.write(LoRaWAN.loraSerial.read());
  }
          
  {
      //*************** Display then transmit data to LoRaWAN
    Serial.print("Sending: ");Serial.println(txData);
    LoRaWAN.LoRaTransmit(0, 30, txData);   //type 0 = Confirmed, 1 = Unconfirmed / port number 1 to 223 / data payload
    delay(20000);
  }
}

