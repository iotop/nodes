# Dragino-Test-915

## Haxiot LoRa shield with Arduino Uno sending test data to hosted Loriot backend

## Hardware required

- Arduino Uno or compatible
- Haxiot US900 Arduino Shield

## Additional software libraries

![Haxiot_LoRaWAN_1.0.1-library for Arduino](https://gitlab.op-bit.nz/BIT/Project/Internet-Of-Things/nodes/tree/master/Haxiot-Test-915/Haxiot_LoRaWAN_1.0.1 "Haxiot_LoRaWAN_1.0.1-library")

## Wiring

![Haxiot915Test](resources/Haxiot915Test.jpg "Haxiot915Test")

None, just install the Arudino shield onto the Arduino board

## Code

![Haxiot915Test](resources/Haxiot915Test.ino "Haxiot915Test")
