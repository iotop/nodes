/** mDot AnalogIn Example Program
 *
 * This program demonstrates how to read an anglog pin using the
 * MultiTech mDot and MultiTech UDK2 hardware. The only
 * additional hardware required is an analog voltage source like
 * a potentiometer.
 *
 *
 * This program reads the analog input connected to pin PB_1 (UDK2
 * pin A0) and prints the result.
 */
 
#include "mbed.h"
 
int main() {
    AnalogIn a0(PB_1);
    
    while (true) {
        //read as float (0.0-1.0, multiply by 1000)
        float ldr1 = a0.read() * 1000;
        wait_ms(100);
        float ldr2 = a0.read() * 1000;
        wait_ms(100);
        float ldr3 = a0.read() * 1000;
        wait_ms(100);
        //average
        float ldr = (ldr1 + ldr2 + ldr3) / 3;
        //output 3 digits . 3 decimal places
        printf("LDR: %3.3f\r\n", ldr);
        wait(1);
    }
}