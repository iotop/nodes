/** mDot AnalogIn Example Program
 *
 * This program demonstrates how to read an anglog pin using the
 * MultiTech mDot and MultiTech UDK2 hardware. The only
 * additional hardware required is an analog voltage source like
 * a potentiometer.
 *
 *
 * This program reads the analog input connected to pin PB_1 (UDK2
 * pin A0) and prints the result.
 * And reads temperature/humidity from a DHT22 sensor and prints
 * the result
 */
 
#include "mbed.h"
#include "DHT22.h"
 
 DHT22 dht22(PA_1);
 
int main() {
    //analogue ldr pin
    AnalogIn a0(PB_1);
    
    
    while (true) {
        printf("-----------------------------\r\n");
        int error = dht22.sample();
        if (error == 1)
        {
            //it's required to divide these values by ten for some reason
            float h = (float)dht22.getHumidity() / 10;
            float t = (float)dht22.getTemperature() / 10;
            printf("TempC: %2.1f, HumidP: %2.1f\r\n", t , h);
        }
        else
        {
            printf("Error: %d\n", error);
        }
        
        //read LDR as float (0.0-1.0, multiply by 1000)
        float ldr1 = a0.read() * 1000;
        wait_ms(100);
        float ldr2 = a0.read() * 1000;
        wait_ms(100);
        float ldr3 = a0.read() * 1000;
        wait_ms(100);
        //average
        float ldr = (ldr1 + ldr2 + ldr3) / 3;
        //output 3 digits . 3 decimal places
        printf("LDR: %3.3f\r\n", ldr);
        wait(5);
    }
}