---
layout: post
title: Getting started with LoRaWAN
---

![LoRaWAN](assets/img/lorawan.png)

### What is LoRaWAN?

>>>
LoRa stands for long range
>>>

LoRaWAN is a low power, wide-area network which is intended for wireless battery operated things in a regional, national or global network. 

In the LoRoWAN network architecture, gateways are connected to the network server via standard IP connections while end-devices use single-hop wireless communication to one or many gateways. 

Low power, wide-area networks have been backed to make up a major portion of the growth of IoT devices in the coming years. Some benefits of LoRa include low range, low power and low cost connectivity as well as security for both devices and networks. 

LoRaWAN is the network on which LoRa operates, and can be used by IoT (Internet of Things) for remote and unconnected industries. 
LoRa and LoRaWAN permit inexpensive, long-range connectivity for Internet of Things devices in rural, remote and offshore locations. They are typically used in mining, natural resource management, renewable energy and supply chain management. 


### LoRaWAN is utilised across many different industries... Some of these include:

#### Solar Panels 

[Enphase Solar Panels](https://enphase.com/en-us)


Enphase Solar Inverter panels sell solar panels and allow customers to pay for these by selling the energy back to the utilities market. 

Initially Enphase used cellular in order to send this data off but have since put MultiTech mDots in the inverter units as it was more cost effective. The mDot aggregates data from the solar panels locally and delivers this data across the network. 

#### Agriculture

[WaterBit](http://www.waterbit.com/)

Use the LoRaWAN network to keep track of moisture levels in the ground

WaterBit use a water probe that senses how moist the water levels in the ground are. It then communicates what the moisture level is in the ground with the platform thus giving the growers more control and allowing them to optimise their harvest. 

#### Digital Signage

[LorkTech](https://bluboard.io/)


Digital signage solution using an E-paper solution the ability to take a large file compress the file and send across a LoRa network compress these signs and have them in a private instance 

#### Space Technology

[Fleet Space Technologies](https://www.fleet.space/) 

Uses LoRaWAN to provide massive connectivity to IoT sensors and devices in rural, remote and offshore areas. 

### In Conclusion

Internet of Things is essentially the use of network-connected devices, embedded in a physical environment to improve an existing process or to enable a new scenario not previously possible. 

These devices, or things, connect to the network to provide information they gather from the environment through sensors. Devices could be devices that you own personally and carry with you or they could be embedded in factory equipment, or part of the the fabric of the city you live in. Each device is able to convert valuable information from the real world into digital data that provides increased visibility into how your users interact with your products, services or applications. 

IoT devices are able to use LoRaWAN networks to transfer this data from the device to a server or platform where it can be stored. 
The fact these networks offer affordable, long range connectivity makes it appropriate for use with IoT devices as their location is often remote due to the industries they are used in. 
