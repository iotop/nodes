# Dragino-TempHumidity-868

## Dragino LoRa shield (868MHz) temperature/humidity example for IoT Dunedin server and 868MHz iC880a based gateway

## Hardware required
1. Arduino Uno or compatible
2. Dragino 868Mhz LoRa Arduino shield 
3. 10KOhm resistor

![Dragino868TempHumid_real.jpg](resources/Dragino868TempHumid_real.jpg "Dragino868TempHumid_real.jpg")

## Additional software libraries

Adafruit DHT library is available in the [DHT-sensor-library directory](https://gitlab.op-bit.nz/BIT/Project/Internet-Of-Things/nodes/blob/master/Dragino-TempHumidity-868/DHT-sensor-library)

## Wiring

Note: Dragino shield not included in diagram but must be installed
Note: Dragino shield leaves pins 0,1,4,5,6 for use

## Code

![Dragino868TempHumid.ino](resources/Dragino868TempHumid.ino "Dragino868TempHumid.ino")