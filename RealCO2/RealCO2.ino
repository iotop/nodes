#include <Adafruit_CircuitPlayground.h>
#include <Adafruit_Circuit_Playground.h>


#include <Arduino.h>
//Ignore all code to do with LEDs and buzzer. That is extra stuff for later once data is reading and sending to loRa server.
#define ANALOGPIN A0                                          // ADC pin which the brown wire is attached to
int redLed = 12;
int greenLed = 11;
int buzzer = 10; 
unsigned long myMHZ19Timer = 0;                            

void setup()
{
    Serial.begin(9600);
    pinMode(ANALOGPIN, INPUT);                         // Pullup A0
    pinMode(redLed, OUTPUT);
    pinMode(greenLed, OUTPUT);
    pinMode(buzzer, OUTPUT);
    Serial.print("\nUsing Pin: ");                            // Print Raw Pin Number
    Serial.println(ANALOGPIN);
                                          
}

void loop()
{
    if (millis() - myMHZ19Timer >= 1000)                      
    {
        Serial.println("-----------------"); 

        float adjustedADC = analogRead(A0);                 
        Serial.print("Analog raw: ");
        Serial.println(adjustedADC);

        adjustedADC = 6.4995 * adjustedADC - 580.53; // format; y=mx+c
        Serial.print("Analog CO2: ");      
        Serial.println(adjustedADC);                                                  
        myMHZ19Timer = millis();
                                   
        if (adjustedADC >= 600)
        {
            digitalWrite(redLed, HIGH);
            digitalWrite(greenLed, LOW);
            tone(buzzer, 1000, 200);
        }
        else
        {
          digitalWrite(redLed, LOW);
          digitalWrite(greenLed, HIGH);
          noTone(buzzer);
        }
    }
}
