# Building LoRaWAN Nodes

Carbon emissions, water waste and energy consumption... What do these things all have in common? They are all being monitered through IoT.

With increased connections and improved access to high-speed networks, IoT has the ability to address and help solve many challenges we face today.

### How is this possible?

End devices, commonly known as nodes transmit data across wide area networks. Networks such as LoRaWAN which we use here in Dunedin are designed to allow low-powered devices to communicate with Internet-connected applications. 

### What are nodes? 

LoRaWAN nodes are objects that contain embedded low-power communication devices.

LoRaWAN nodes have two main roles: 

1) Sensing the environment around them; this is achieved through the use of a sensor and can be used to track things such as water levels or humidity;

2) Send data through the LoRaWAN network, to the end user. 


# Devices 

In the Internet of Things (IoT) Dunedin group we spend the majority of our time working with the following devices: 

## Dragino 
The LoRa Shield allows the user to send data and reach extremely long ranges at low data-rates.
It provides ultra-long range spread spectrum communication and high interference immunity whilst minimising current consumption.

The Lora Shield targets professional wireless sensor network applications such as irrigation systems, smart metering, smart cities, smartphone detection, building automation, and so on.

![Dragino](Img/DraginoOverview.png)

You can buy the Dragino LoRa Shield [here](https://www.amazon.com/Seeedstudio-Dragino-LoRa-Shield-frequency/dp/B01GFV59RQ)


## mDot
The mDot is capable of 2-way single-duplex communication over distances of up to 10 miles/16 km, line of sight, 
deep into buildings or within noisy environments in North America, Europe, and worldwide.

![mDot](Img/mdot.png)

You can buy the MultiTech MDot here [here](https://www.amazon.com/Seeedstudio-Dragino-LoRa-Shield-frequency/dp/B01GFV59RQ)

## Adafruit Feather 32u4

![AdafruitFeather](Img/adafruitFeather.jpg)

We're using the adafruit feather device for our room sensors. We use this device along with a DHT22 sensor to measure room temperature and humidity for various classrooms around Polytech

# Sensors

## DHT22 

The DHT22 is a low cost digital temperature and humidity sensor with a single wire digital interface. It uses a capacitive humidity sensor 
and a thermistor to measure the surrounding air, and spits out a digital signal on the data pin (no analog input pins needed). 

![DHT22](Img/dht22-sensor.png)


# Building LoRaWAN Nodes

### Adafruit Room Sensor 

What we need:
- Adafruit Feather 32u4 device
- DHT22 Sensor

[Getting started tutorial](https://gitlab.op-bit.nz/BIT/Project/Internet-Of-Things/nodes/tree/master/AdafruitFeather32u4-ABP-ClassroomSensor-AU915)

Purchase [Here](https://www.adafruit.com/product/2771) 


### Dragino LoraShield

- Dragino LoRa Shield AU915
- Arduino Uno board
- Antenna

[Getting started tutorial](https://gitlab.op-bit.nz/BIT/Project/Internet-Of-Things/nodes/tree/master/Dragino-ABP-HelloWorld-AU915)

Purchase [Here](https://www.amazon.com/Seeedstudio-Dragino-LoRa-Shield-frequency/dp/B01GFV59RQ)

### Multitech MDot 

- mDot - 915
- MultiTech mDot UDK board
- 900MHz antenna

[Getting started tutorial](https://gitlab.op-bit.nz/BIT/Project/Internet-Of-Things/nodes/tree/master/mDot-HelloWorld-915)

Purchase [Here]()